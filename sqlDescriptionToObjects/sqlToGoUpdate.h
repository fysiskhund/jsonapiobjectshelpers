#ifndef SQLTOGOUPDATE_H
#define SQLTOGOUPDATE_H

#include <string>
#include <vector>

#include "macros.h"


using namespace  std;

void sqlToGoUpdate(unsigned numFields, string& objectName, string& tableName, string& instanceName, string& instancesName,
                   vector<string>& fields, vector<string>& fieldsCapitalised, vector<string>& types,
                   vector<string>& typesGo, vector<string>& typesJava, vector<bool>& nullables) {

    printf("var upd%s objects.%s\n", objectName.c_str(), objectName.c_str());
    printf("decode%sError := json.NewDecoder(r.Body).Decode(&upd%s)\n", objectName.c_str(), objectName.c_str());
    printf("if decode%sError != nil {\n", objectName.c_str());
    printf(" w.WriteHeader(http.StatusBadRequest)\n return\n}\n");

    printf("update%s, update%sError := objects.DBConnection.Prepare(\"UPDATE %s SET ", objectName.c_str(), objectName.c_str(), tableName.c_str());

    for( unsigned i = 0; i < numFields; i++) {
        printf(" `%s`=?%s", fields.at(i).c_str(), AUTOCOMMA);
    }

    printf("\")\n");
    printf("defer update%s.Close()\n", objectName.c_str());

    printf("if update%sError != nil {\n w.WriteHeader(http.StatusBadRequest)\n return\n}\n", objectName.c_str());


    printf("_, update%sExecError := update%s.Exec(",objectName.c_str(), objectName.c_str());
    for( unsigned i = 0; i < numFields; i++) {
        printf("upd%s.%s,%s", objectName.c_str(), fieldsCapitalised.at(i).c_str(), AUTOLINEBREAK);
    }
    printf(")\n");
    printf("if update%sExecError != nil {\n", objectName.c_str(), objectName.c_str());
    printf(" w.WriteHeader(http.StatusInternalServerError)\n return\n}");



}

#endif // SQLTOGOUPDATE_H
