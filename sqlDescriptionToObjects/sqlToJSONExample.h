#ifndef SQLTOJSONEXAMPLE_H
#define SQLTOJSONEXAMPLE_H

#include <string>
#include <vector>

#include "macros.h"


using namespace  std;

void sqlToJSONExample(unsigned numFields,
                   vector<string>& fields, vector<string>& types) {
    printf("{\n");
    for (unsigned i = 0; i < numFields; i++) {
        printf("\"%s\": %s%s\n", fields.at(i).c_str(), LOREMIPSUM, AUTOCOMMA);
    }
    printf("}\n");

}
#endif // SQLTOJSONEXAMPLE_H
