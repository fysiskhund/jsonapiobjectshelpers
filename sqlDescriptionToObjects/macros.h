#ifndef MACROS_H
#define MACROS_H

#define EXACT_MATCH 0
#define AUTOCOMMA i< (numFields-1)? ",":""
#define AUTOLINEBREAK (i+1) % 3? "":"\n"

#define LOREMIPSUM types.at(i).find("INT") != string::npos? "1234": "\"lorem\""


#endif // MACROS_H
