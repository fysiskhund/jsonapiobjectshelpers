#ifndef SQLTOGOINSERT_H
#define SQLTOGOINSERT_H

#include <string>
#include <vector>

#include "macros.h"


using namespace  std;

void sqlToGoInsert(unsigned numFields, string& objectName, string& tableName, string& instanceName, string& instancesName,
                   vector<string>& fields, vector<string>& fieldsCapitalised, vector<string>& types,
                   vector<string>& typesGo, vector<string>& typesJava, vector<bool>& nullables) {

    printf("var new%s objects.%s\n", objectName.c_str(), objectName.c_str());
    printf("decode%sError := json.NewDecoder(r.Body).Decode(&new%s)\n", objectName.c_str(), objectName.c_str());
    printf("if decode%sError != nil {\n", objectName.c_str());
    printf(" w.WriteHeader(http.StatusBadRequest)\n return\n}\n");

    printf("insert%s, insert%sError := objects.DBConnection.Prepare(\"INSERT INTO %s(", objectName.c_str(), objectName.c_str(), tableName.c_str());

    for( unsigned i = 0; i < numFields; i++) {
        printf(" `%s`%s", fields.at(i).c_str(), AUTOCOMMA);
    }
    printf(") VALUES(");
    for( unsigned i = 0; i < numFields; i++) {
        printf("?,");
    }
    printf(")\")\n");
    printf("defer insert%s.Close()\n", objectName.c_str());

    printf("if insert%sError != nil {\n w.WriteHeader(http.StatusBadRequest)\n return\n}\n", objectName.c_str());


    printf("_, insert%sExecError := insert%s.Exec(",objectName.c_str(), objectName.c_str());
    for( unsigned i = 0; i < numFields; i++) {
        printf("new%s.%s,%s", objectName.c_str(), fieldsCapitalised.at(i).c_str(), AUTOLINEBREAK);
    }
    printf(")\n");
    printf("if insert%sExecError != nil {\n", objectName.c_str(), objectName.c_str());
    printf(" w.WriteHeader(http.StatusInternalServerError)\n return\n}");



}


#endif // SQLTOGOINSERT_H
