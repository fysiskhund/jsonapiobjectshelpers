#include <iostream>
#include <vector>

#include "macros.h"
#include "sqlToGoInsert.h"
#include "sqlToGoUpdate.h"
#include "sqlToJSONExample.h"

using namespace std;



enum ParseStatus {
    PARSE_NONE,
    PARSE_START,

    PARSE_OBJECT,
    PARSE_PREWORD,
    PARSE_WORD,
    PARSE_POSTWORD,
    PARSE_LINE,

    PARSE_FIELD,
    PARSE_PRETYPE,
    PARSE_TYPE,
    PARSE_NULLABLE
};
int main()
{
    FILE* inFile = fopen("infile.sql", "r");

    string line;

    string parsedWord;
    string parsedWordCapitalised;
    string objectName;
    string tableName;
    string instanceName;
    string instancesName;

    vector<string> fields;
    vector<string> fieldsCapitalised;
    vector<string> types;
    vector<string> typesGo;
    vector<string> typesJava;
    vector<bool> nullables;

    string goLine;
    int goPos = 0;
    string goName;


    string javaLine;


    char charChar;
    int pos = 0;

    bool capitalise = true;

    int parseState = PARSE_START;
    int parseEntity = PARSE_NONE;

    while( fread(&charChar, 1, 1, inFile)) {
        line += charChar;


        switch (parseState) {
        case PARSE_START: {
            if (line.compare("CREATE TABLE") == EXACT_MATCH) {
                parseState = PARSE_PREWORD;
                parseEntity = PARSE_OBJECT;
                parsedWord = "";
                parsedWordCapitalised = "";
            }

        } break;
            
        case PARSE_PREWORD: {
            if (charChar == ' ' || charChar == '\n') {
                // ignore
            } else {
                parseState = PARSE_WORD;

                parsedWord += charChar;
                charChar = toupper(charChar);
                parsedWordCapitalised += charChar;

                capitalise = false;
            }
        }break;
        case PARSE_WORD: {
            if (charChar == ' ' || charChar == '(' || charChar == ',') {
                // end of word
                
                switch(parseEntity) {
                case PARSE_OBJECT:{
                    objectName = parsedWordCapitalised;
                    tableName = parsedWord;

                    // Plural instancesname so it can be used in e.g. Go slices
                    instancesName = tableName.substr(0,1) + objectName.substr(1);

                    // Snip the last 's' to turn plural into singular.
                    if (objectName.c_str()[objectName.length()-1] == 's'){
                        objectName = objectName.substr(0, objectName.length()-1);

                    }
                    instanceName = tableName.substr(0,1) + objectName.substr(1);


                    parsedWord = "";
                    parsedWordCapitalised = "";
                    parseState = PARSE_POSTWORD; // Search for EOL.
                    
                }break;
                case PARSE_FIELD:{
                    fields.push_back(parsedWord);
                    fieldsCapitalised.push_back(parsedWordCapitalised); // push fieldname to stack
                    parsedWord = "";
                    parsedWordCapitalised = "";

                    parseState= PARSE_PREWORD;
                    parseEntity = PARSE_TYPE;
                }break;
                case PARSE_TYPE:{
                    types.push_back(parsedWord); // Push type to stack
                    parsedWord = "";
                    parsedWordCapitalised = "";
                    parseState = PARSE_POSTWORD; //
                    parseEntity = PARSE_NULLABLE;
                }break;
                // Nullable is not processed here
                }

            } else if (charChar == '_') {
                capitalise = true;
                parsedWord += charChar; // Not ignored in lowercase fields!
            } else if (charChar == ','){
                // ignore
            } else {

                parsedWord += charChar;
                if (capitalise) {
                    capitalise = false;
                    charChar = toupper(charChar);
                }
                parsedWordCapitalised += charChar;
            }
        }break;
        case PARSE_POSTWORD:
            if (charChar == '\n') {
                switch(parseEntity) {
                case PARSE_OBJECT: {
                    printf("Object name: %s\n", objectName.c_str());
                    parseState = PARSE_PREWORD;
                    parseEntity = PARSE_FIELD;
                    parsedWord = "";
                    parsedWordCapitalised = "";
                }break;
                case PARSE_TYPE: {
                    //printf("Field: %s, Type: %s\n", fieldsCapitalised.at(fieldsCapitalised.size()-1).c_str(), types.at(types.size()-1).c_str());
                    parseState = PARSE_POSTWORD;
                    parseEntity = PARSE_NULLABLE;
                    parsedWord = "";
                    parsedWordCapitalised = "";
                }break;

                case PARSE_NULLABLE: {
                    bool isNullable = (parsedWord.find("NOT NULL") == string::npos);

                    nullables.push_back(isNullable);


                    printf("Field: %s, Type: %s Nullable: %s\n", fieldsCapitalised.at(fieldsCapitalised.size()-1).c_str(), types.at(types.size()-1).c_str(), isNullable? "yes":"nope");
                    parseState = PARSE_PREWORD;
                    parseEntity = PARSE_FIELD;
                    parsedWord = "";
                    parsedWordCapitalised = "";
                }break;
                }
            } else {
                parsedWord += charChar;
            }
        }
    }
    unsigned long numFields = fields.size()-1; // Last one is PRIMARY KEY blahblah

    // Process Go types
    for (unsigned long i = 0; i < numFields; i++) {
        string goFieldStr;
        if (nullables.at(i)) {
            goFieldStr = "*";
        }
        if (types.at(i).compare("INT") == EXACT_MATCH) {
            goFieldStr = "int"; // int fields are not nullable
        } else if (types.at(i).compare("VARCHAR") == EXACT_MATCH) {
            goFieldStr += "string";
        } else if (types.at(i).compare("DATETIME") == EXACT_MATCH) {
            goFieldStr = "time.Time"; // datetime is not nullable
        }
        typesGo.push_back(goFieldStr);

    }

    // Process Java types
    for (unsigned long i = 0; i < numFields; i++) {
        string javaFieldStr;

        if (types.at(i).compare("INT") == EXACT_MATCH) {
            javaFieldStr = "int"; // int fields are not nullable
        } else if (types.at(i).compare("VARCHAR") == EXACT_MATCH) {
            javaFieldStr += "String";
        } else if (types.at(i).compare("DATETIME") == EXACT_MATCH) {
            javaFieldStr = "Date"; // datetime is not nullable
        }
        typesJava.push_back(javaFieldStr);

    }


    printf("\n\n");
    printf(" ** Go object **\n");


    printf("package objects\n");
    printf("type %s struct {\n", objectName.c_str());

    for (unsigned long i = 0; i < numFields; i++) {
        printf(" %s  %s  `json:\"%s\"`\n", fieldsCapitalised.at(i).c_str(), typesGo.at(i).c_str(), fields.at(i).c_str());
    }
    printf("}\n");
    printf("\n\n");

    printf(" ** Go SQL query **\n\n");
    printf("queryStr := \"SELECT");
    for (unsigned long i = 0; i < numFields; i++) {
        printf(" %s%s", fields.at(i).c_str(), AUTOCOMMA);
    }
    printf(" \"\n");
    printf("queryStr += \"FROM %s WHERE <condition>\" // FIXME: fill-in conditions\n", tableName.c_str());

    printf("result, selectError := objects.DBConnection.Query(queryStr)\n");
    printf("defer result.Close()\n\n");

    printf(" if selectError == nil {\n");
    printf(" var %sSlice []objects.%s\n", instancesName.c_str(), objectName.c_str());
    printf("   for result.Next() {\n");

    printf("    var %s objects.%s\n", instanceName.c_str(), objectName.c_str());
    printf("    resultError := result.Scan(\n");
    for (unsigned long i = 0; i < numFields; i++) {
        printf("&%s.%s,%s", instanceName.c_str(), fieldsCapitalised.at(i).c_str(), AUTOLINEBREAK);
    }
    printf(")\n");
    printf("    if resultError != nil {\n    log.Printf(\"Error when scanning results: %%s\", resultError.Error())\n}\n");
    printf(" %sSlice = append(%sSlice, %s)\n", instancesName.c_str(), instancesName.c_str(), instanceName.c_str());
    printf("// FIXME: do something here\n}");



    printf("\n\n");

    printf(" ** Go SQL insertion query **\n");
    sqlToGoInsert(numFields, objectName, tableName, instanceName, instancesName, fields, fieldsCapitalised, types, typesGo, typesJava, nullables);

    printf("\n\n");

    printf(" ** Go SQL update query **\n");
    sqlToGoUpdate(numFields, objectName, tableName, instanceName, instancesName, fields, fieldsCapitalised, types, typesGo, typesJava, nullables);

    printf("\n\n");

    printf(" ** Sample JSON object **\n");
    sqlToJSONExample(numFields, fields, types);

    printf("\n\n");


    printf(" ** Java object **\n");
    printf("public static class %s {\n", objectName.c_str());
    for (unsigned long i = 0; i < numFields; i++) {
        printf("    public %s  %s;\n", typesJava.at(i).c_str(), fields.at(i).c_str());
    }
    printf("};\n");

    return 0;
}
