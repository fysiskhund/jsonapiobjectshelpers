TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp

HEADERS += \
    macros.h \
    sqlToGoInsert.h \
    sqlToGoUpdate.h \
    sqlToJSONExample.h
